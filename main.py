from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputState
from st3m.reactor import Responder
from ctx import Context
import st3m.run
import os

import leds

button_leds = [
    [37,38,39,0,1,2,3], # top north
    [2,3,4,5,6], # buttom north east
    [5,6,7,8,9,10,11], # top east
    [10,11,12,13,14], # buttom east
    [13,14,15,16,17,18,19], # top south east
    [18,19,20,21,22], # buttom south
    [21,22,23,24,25,26,27], # top south west
    [26,27,28,29,30], # buttom west
    [29,30,31,32,33,34,35], # top west
    [34,35,36,37,38], # buttom north west
]

top_menu_emojis = [
    '1f642', # happy
    '1f603', # laugh
    '1f60e', # cool
    '1f615', # sad
    '1f970', # love
]

sub_menu_emojis = [
    [ # happy
        '1f600', # grinning
        '1f60c', # relieved
        '1f63a', # smile cat
        '1f60a', # blush
        '1f604', # eyes closed smile
    ],
    [ # laugh
        '1f602', # tears of joy
        '1f606', # grin
        '1f639', # tears of joy cat
        '1f92a', # crazy
        '1f973', # party
    ],
    [ # cool
        '1f607', # halo
        '1f974', # weird
        '1f92a', # crazy
        '1f973', # party
        '1f608', # evil
    ],
    [ # sad
        '1f614', # pensive
        '1f61e', # disappointed
        '1f622', # cry
        '1f62d', # cry cat
        '1f62a', # angry
    ],
    [ # love
        '1f60d', # heart eyes
        '1f618', # heart kiss
        '1f63b', # heart eyes cat
        '1f619', # closed eyed kiss
        '1f917', # hug
    ],
]

emoji_colors = {
    # happy
    '1f642': (255,255,0),
    '1f600': (255,215,0), # grinning
    '1f60c': (255,0,0), # relieved
    '1f63a': (255,0,255), # smile cat
    '1f60a': (255, 0, 127), # blush
    '1f604': (255, 127, 0), # eyes closed smile
    # laugh
    '1f603': (255, 255, 0),
    '1f602': (202, 34, 96), # tears of joy
    '1f606': (255, 215, 0), # grin
    '1f639': (255, 0, 255), # tears of joy cat
    '1f92a': (255, 0, 127), # crazy
    '1f973': (255, 127, 0), # party
    # cool
    '1f60e': (0, 0, 255),
    '1f607': (255, 200, 220), # halo
    '1f974': (255, 215, 0), # weird
    '1f92a': (255, 0, 255), # crazy
    '1f973': (255, 0, 127), # party
    '1f608': (255, 0, 255), # evil
    # sad
    '1f615': (0, 0, 255),
    '1f614': (255, 215, 0), # pensive
    '1f61e': (202, 34, 96), # disappointed
    '1f622': (255, 0, 255), # cry
    '1f62d': (255, 0, 127), # cry cat
    '1f62a': (255, 127, 0), # angry
    # love
    '1f970': (255, 0, 255),
    '1f60d': (240, 40, 0), # heart eyes
    '1f618': (240, 0, 40), # heart kiss
    '1f63b': (255, 0, 0), # heart eyes cat
    '1f619': (140, 0, 20), # closed eyed kiss
    '1f917': (255, 100, 100), # hug
    
}

# switch off all leds
def leds_off():
    for i in range(0, 40):
        leds.set_rgb(i, 0, 0, 0)

def leds_all_color(r: int, g: int, b: int):
    for i in range(0, 40):
        leds.set_rgb(i, r, g, b)


# switch on the leds for the button
def leds_petal_on(petal: int, r: int, g: int, b: int):
    for i in button_leds[petal]:
        leds.set_rgb(i, r, g, b)

class App(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        
        self.i = 20
        self.current = 0
        self.last = 0

        self.emoji_dir = os.path.join(app_ctx.bundle_path,'assets/emojis')
        self.emojis = os.listdir(self.emoji_dir)

        self.emoji = '1f63b'

        self.top_menu_selection = 0

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-144, -144, 288, 288).fill()
        emoji_filename = next((x for x in self.emojis if x.startswith(self.emoji)))
        if emoji_filename:
            ctx.image(os.path.join(self.emoji_dir,emoji_filename), -80, -80, 160, 160)


    def think(self, ins: InputState, delta_ms: int) -> None:
        self.current += delta_ms

        direction = ins.buttons.app

        if direction == ins.buttons.PRESSED_LEFT:
            if (self.current - self.last) > 100:
                self.last = self.current

                self.i -= 1
                if self.i < 0:
                    self.i = len(self.emojis) - 1

                self.emoji = self.emojis[self.i]

        elif direction == ins.buttons.PRESSED_RIGHT:
            if (self.current - self.last) > 100:
                self.last = self.current

                self.i += 1
                if self.i >= len(self.emojis):
                    self.i = 0
                
                self.emoji = self.emojis[self.i]
        
        if self.emoji in emoji_colors:
            leds_all_color(*emoji_colors[self.emoji])
        else:
            leds_off()

        for i in range(0, 10):
            petal = ins.captouch.petals[i]
            if petal.pressed:
                if not petal.bottom:
                    self.top_menu_selection = int(i/2)
                    # print('top menu',self.top_menu_selection)
                    self.emoji = top_menu_emojis[self.top_menu_selection]
                    leds_petal_on(i,255,0,0)
                else:
                    self.sub_menu_selection = int(i/2)
                    # print('sub menu',self.sub_menu_selection)
                    self.emoji = sub_menu_emojis[self.top_menu_selection][self.sub_menu_selection]
                    leds_petal_on(i,0,0,255)
        leds.update()

if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(App(ApplicationContext(bundle_path='/flash/sys/apps/3moji')))
