#!/bin/bash

# fail if any commands fails
set -e

# go to the script directory
cd "$(dirname "$0")"

mkdir -p emojis

# read emoji.list
EMOJIS=`cat emojis.list`

# convert emoji to png
for emoji in $EMOJIS
do
    echo "convert $emoji"
    #convert -background none -density 600 -resize 160x160 twemoji/assets/svg/$emoji.svg emojis/$emoji.png
    cp twemoji/assets/72x72/$emoji.png emojis/$emoji.png
done
